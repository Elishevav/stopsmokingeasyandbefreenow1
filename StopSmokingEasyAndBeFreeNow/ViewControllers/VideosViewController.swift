//
//  VideosViewController.swift
//  StopSmokingEasyAndBeFreeNow
//
//  Created by Elisheva Vakrat on 21/08/2019.
//  Copyright © 2019  All rights reserved.
//

import UIKit
import AVKit
import WebKit


class VideosViewController: UIViewController {
    @IBOutlet weak var myWebView: WKWebView!

     var levels : LevelDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         GetVideo()

    }
    
    func GetVideo()
    {
        let url = URL(string: levels!.VideoName)
        myWebView.load(URLRequest(url: url!))
        
    }
    
    @IBAction func BackButton(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
}
    
